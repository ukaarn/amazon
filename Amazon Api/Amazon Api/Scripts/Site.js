﻿$(document).ready(function () {
    var t = $('#Results').DataTable({
        "lengthMenu": [[13], [13]],
        "pagingType": "simple_numbers",
        "lengthChange": false,
        "filter": false,
        "ordering": false,
        "info": false
    });

    $(function () {
        var progressbar = $("#progressbar"), progressLabel = $(".progress-label");
        progressbar.progressbar({

        });
    });

    $('#search').on('click', function () {
        $('#loading').show();
        t.clear().draw();
        getItems(1, t);
    });
});

function getItems(page, t) {

    $(function () {$("#progressbar").progressbar("value", 20* page);});


    if (page > 5) {
        $('#loading').hide();
        return;
    }
        
    var URL = "/amazonapi/GetItems/" + $("#phrase").val() + "/"+page+"/";
    $.get(URL, function (data) {
        $.each(data, function (index, d) {
            if (d.image != null) {
                t.row.add(['<img src="'+ d.image.url +'" width="'+ d.image.width+'" height="'+ d.image.height+'"/>', d.title, d.productType, d.price]).draw();
            }
        });
        setTimeout(getItems(page + 1, t), 1000);
    });
    
}