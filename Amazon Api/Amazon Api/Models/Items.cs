﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace Amazon_Api.Models
{
    public class Items{

        private List<Item> items = new List<Item>();
        private const string ASSOCIATE_TAG = "praktika-20";
        private const string ACCESS_ID = "AKIAJ7U4E4IPEQKAVSDQ";
        private const string SECRET = "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";
        public void getItemByPhrase(string searchPhrase, string page) {
            items = new List<Item>();
            string url = generateSignedURL(searchPhrase, page);
            System.IO.File.WriteAllText(@"C:\Users\Public\WriteText.txt", url);
            parseXmlFromUrl(url, items);
        }

        public List<Item> getItemsList() {
            return items;
        }

        private void parseXmlFromUrl(string url, List<Item> items) {
            Item item = new Item();
            XmlDocument XML = new XmlDocument();
            XML.Load(url);
            XmlNamespaceManager xmlnm = new XmlNamespaceManager(XML.NameTable);
            xmlnm.AddNamespace("ns", "http://webservices.amazon.com/AWSECommerceService/2011-08-01");
            XmlNodeList totalResults = XML.SelectNodes("//ns:TotalResults", xmlnm);
            XmlNodeList nodes = XML.SelectNodes("//ns:MediumImage | //ns:ProductTypeName | //ns:Title | //ns:FormattedPrice", xmlnm);
            foreach (XmlNode node in nodes) {
                item = parseXmlNode(node, item, items);
            }
        }

        private Item parseXmlNode(XmlNode node, Item item, List<Item> items) {
            if (node.Name == "MediumImage") {
                item = new Item();
                XmlNodeList imageInfo = node.ChildNodes;
                Image image = new Image();
                image.url = imageInfo[0].InnerText;
                image.width = imageInfo[2].InnerText;
                image.height = imageInfo[1].InnerText;
                item.image = image;
            } else if (node.Name == "FormattedPrice") {
                item.price = node.InnerText;
            } else if (node.Name == "ProductTypeName") {
                item.productType = node.InnerText;
            } else if (node.Name == "Title") {
                item.title = node.InnerText;
                items.Add(item);
            }
            return item;
        }

        private string generateSignedURL(string keywords, string pageNr) {
            string url = "http://ecs.amazonaws.com/onca/xml?" +
                generateUrlParameters(keywords, pageNr) +
                "&Signature=" + generateSignature(generateUrlParameters(keywords, pageNr));
            return url;
        }

        private string generateSignature(string signMe) {
            signMe = "GET\necs.amazonaws.com\n/onca/xml\n" + signMe;
            byte[] bytesToSign = Encoding.UTF8.GetBytes(signMe);
            byte[] secretKeyBytes = Encoding.UTF8.GetBytes(SECRET);
            HMAC hmacSha256 = new HMACSHA256(secretKeyBytes);
            byte[] hashBytes = hmacSha256.ComputeHash(bytesToSign);
            return Uri.EscapeDataString(Convert.ToBase64String(hashBytes));
        }

        private string generateUrlParameters(string keywords, string pageNr) {
            string parameters = "AWSAccessKeyId=" + ACCESS_ID +
                "&AssociateTag=" + ASSOCIATE_TAG +
                "&ItemPage=" + pageNr +
                "&Keywords=" + Uri.EscapeDataString(keywords) +
                "&Operation=ItemSearch" +
                "&ResponseGroup=Images%2CItemAttributes" +
                "&SearchIndex=All" +
                "&Service=AWSECommerceService" +
                "&Timestamp=" + Uri.EscapeDataString(getTimeStamp()) +
                "&Version=2011-08-01";
            return parameters;

        }

        private string getTimeStamp() {
            DateTime now = DateTime.UtcNow;
            string timestamp = now.ToString("yyyy-MM-ddTHH:mm:ssZ");
            return timestamp;
        }

    }
}