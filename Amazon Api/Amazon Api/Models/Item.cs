﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Amazon_Api.Models
{
    public class Image {
        public string url { get; set; }
        public string width { get; set; }
        public string height { get; set; }

        public String getImage() {
            return "<img src=\"" + url +
                "\" width=\"" + width +
                "\" heigth=\"" + height + "\">";
        }
    }

    public class Item {

        public Image image { get; set; }
        public string productType { get; set; }
        public string title { get; set; }
        public string price { get; set; }

        public string getProduct() {
            return "<tr>\n" +
                "\t<td>" + image.getImage() + "</td>\n" +
                "\t<td>" + title + "</td>\n" +
                "\t<td>" + productType + "</td>\n" +
                "\t<td>" + price + "</td>\n" +
                    "</tr>";
        }
    }
}