﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Amazon_Api.Models;
namespace Amazon_Api.Controllers {
    public class AmazonAPIController : Controller {
        public JsonResult GetItems(string searchPhrase, string page) {
            Items items = new Items();
            items.getItemByPhrase(searchPhrase, page);
            return Json(items.getItemsList(), JsonRequestBehavior.AllowGet);
        } 
    }
}
