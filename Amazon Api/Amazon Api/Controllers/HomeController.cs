﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Amazon_Api.Models;

namespace Amazon_Api.Controllers {
    public class HomeController : Controller {
        public ActionResult Index() {
            return View(); 
        }
    }
}
